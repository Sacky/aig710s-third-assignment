### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 134b5e71-96a1-4240-8f16-fd31570e56c7
md"## MDP Program"

# ╔═╡ f710c5a1-ddda-4895-a170-72ac1df2d18b
md" State"

# ╔═╡ 579dbedc-9eb6-40eb-89ac-a275f06caf30
states = ["up", "down","left", "right"]

# ╔═╡ 8598d24c-969c-42a0-955d-58c28d0288e3
md" Action"

# ╔═╡ bf790acb-afd3-4136-a911-e9641cd912c3
actions = ["up", "down","left", "right", "listen"]

# ╔═╡ 82cd07b8-9bc1-4af4-bf9e-16983e0c257c
md" Observation"

# ╔═╡ 6d547173-f8b0-4df0-b158-767cf0aa8ba9
observations = ["up", "down","left", "right"]

# ╔═╡ 863ffd85-b638-4a3e-bdab-b8b8e8afe716
md" Discount"

# ╔═╡ 1ae4c526-ab8f-4e18-ba31-833689e72a78
discount = 0.7

# ╔═╡ 8cadd647-9afd-4d29-8b2a-dcb2788fd204
min = 0

# ╔═╡ 931bdeda-f92c-4fab-8dea-165bffc0da0c
max = 100

# ╔═╡ 196e2f23-86f1-4134-8ef5-091a4989b366
step = 10

# ╔═╡ c4187b8b-ad08-4a16-ab2d-0f16037f7690
md"discrete state variable"

# ╔═╡ 8f2e97cb-3ab4-45c0-8c91-16b036741438
actions(mdp, "move", ["up", "down", "left", "right"])

# ╔═╡ 03d76113-7b4c-4491-a1c7-949fa1345882
md" range or the array of possible values for the state variable"

# ╔═╡ c95c66b1-13b5-48f7-819d-3254c91d9450
states(mdp, "x", min, max)  # continuous

# ╔═╡ a1e6d2e3-81c4-4cd8-bbaa-7c8f5332661c
states(mdp, "goal", ["yes", "no"])  # discrete

# ╔═╡ 937d9e80-ba93-42c0-9579-906ad859f5b3
md"action variables"

# ╔═╡ 3701f3c8-5f81-4354-a8c9-78859fb47397
actions(mdp, "move", ["R", "L", "stop"])  # discrete

# ╔═╡ 6d06f503-279e-4cef-9cba-d2b02e4cadec
function isgoal(x::Float64)
  if abs(x - max / 2) < stepX
    return "yes"
  else
    return "no"
  end
end

# ╔═╡ 782b4147-0e06-4ef6-aec5-b7e3db6b8724
md"## Transition"

# ╔═╡ a18fcc71-d258-4124-be3b-03c9cdb33d99
function transition(x::Float64, goal::AbstractString, move::AbstractString)
  if isgoal(x) == "yes" && goal == "yes"
    return [([x, isgoal(x)], 1.0)]
  end

  if move == "L"
    if x >= max
      return [
        ([x, isgoal(x)], 0.9),
        ([x - step, isgoal(x - StepX)], 0.1)]
    elseif x <= min
      return [
        ([x, isgoal(x)], 0.2),
        ([x + step, isgoal(x + step)], 0.8)]
    else
      return [
        ([x, isgoal(x)], 0.1),
        ([x - step, isgoal(x - step)], 0.1),
        ([x + StepX, isgoal(x + step)], 0.8)]
    end
  elseif move == "R"
    if x >= max
      return [
        ([x, isgoal(x)], 0.1),
        ([x - step, isgoal(x - step)], 0.9)]
    elseif x <= min
      return [
        ([x, isgoal(x)], 0.9),
        ([x + step, isgoal(x + step)], 0.1)]
    else
      return [
        ([x, isgoal(x)], 0.1),
        ([x - step, isgoal(x - step)], 0.8),
        ([x + step, isgoal(x + step)], 0.1)]
    end
  elseif move == "stop"
    return [([x, isgoal(x)], 1.0)]
  end
end

# ╔═╡ 780a2d86-dceb-4fca-b55f-4014e1a9ae18
transition(mdp, ["x", "goal", "move"], mytransition)

# ╔═╡ 6fda546d-133b-4d19-a748-70905e9c1555
md"## Reward"

# ╔═╡ b079fcb4-5cc6-418b-8c3a-0377356acae6
function reward(x::Float64, goal::AbstractString, move::AbstractString)
  if goal == "yes" && move == "stop"
    return 1
  else
    return 0
  end
end

# ╔═╡ 2376f923-68e7-40c5-89f6-13631e899e6d
reward!(mdp, ["x", "goal", "move"], reward)

# ╔═╡ f2880860-b6bf-4d2a-9865-999f1c964f31
solver = SerialValueIteration()

# ╔═╡ b5f3598e-4dc3-479b-a3de-a87dd3d539c9
solution = solve(mdp, solver)

# ╔═╡ 7241ae4b-a1eb-4342-920c-ea75c784977c
md" Generating the optimal policy"

# ╔═╡ 51eb266f-dd19-4c81-aea8-be271bfadd10
policy = getpolicy(mdp, solution)

# ╔═╡ 117bc9be-edd5-4614-a7d0-e975915a15bc
begin
	stateq = (12, "no")
    actionq = policy(stateq...)
end

# ╔═╡ bc9eff50-dcda-11eb-2075-c9fcd0b8d41d
mdp =(S,A,T,R)

# ╔═╡ caf76aca-34b7-4b93-ad1b-9831aef9d2c7
mdp = MDP()

# ╔═╡ Cell order:
# ╠═134b5e71-96a1-4240-8f16-fd31570e56c7
# ╠═f710c5a1-ddda-4895-a170-72ac1df2d18b
# ╠═579dbedc-9eb6-40eb-89ac-a275f06caf30
# ╠═8598d24c-969c-42a0-955d-58c28d0288e3
# ╠═bf790acb-afd3-4136-a911-e9641cd912c3
# ╠═82cd07b8-9bc1-4af4-bf9e-16983e0c257c
# ╠═6d547173-f8b0-4df0-b158-767cf0aa8ba9
# ╠═863ffd85-b638-4a3e-bdab-b8b8e8afe716
# ╠═1ae4c526-ab8f-4e18-ba31-833689e72a78
# ╠═8cadd647-9afd-4d29-8b2a-dcb2788fd204
# ╠═931bdeda-f92c-4fab-8dea-165bffc0da0c
# ╠═196e2f23-86f1-4134-8ef5-091a4989b366
# ╠═caf76aca-34b7-4b93-ad1b-9831aef9d2c7
# ╠═bc9eff50-dcda-11eb-2075-c9fcd0b8d41d
# ╠═c4187b8b-ad08-4a16-ab2d-0f16037f7690
# ╠═8f2e97cb-3ab4-45c0-8c91-16b036741438
# ╠═03d76113-7b4c-4491-a1c7-949fa1345882
# ╠═c95c66b1-13b5-48f7-819d-3254c91d9450
# ╠═a1e6d2e3-81c4-4cd8-bbaa-7c8f5332661c
# ╠═937d9e80-ba93-42c0-9579-906ad859f5b3
# ╠═3701f3c8-5f81-4354-a8c9-78859fb47397
# ╠═6d06f503-279e-4cef-9cba-d2b02e4cadec
# ╠═782b4147-0e06-4ef6-aec5-b7e3db6b8724
# ╠═a18fcc71-d258-4124-be3b-03c9cdb33d99
# ╠═780a2d86-dceb-4fca-b55f-4014e1a9ae18
# ╠═6fda546d-133b-4d19-a748-70905e9c1555
# ╠═b079fcb4-5cc6-418b-8c3a-0377356acae6
# ╠═2376f923-68e7-40c5-89f6-13631e899e6d
# ╠═f2880860-b6bf-4d2a-9865-999f1c964f31
# ╠═b5f3598e-4dc3-479b-a3de-a87dd3d539c9
# ╠═7241ae4b-a1eb-4342-920c-ea75c784977c
# ╠═51eb266f-dd19-4c81-aea8-be271bfadd10
# ╠═117bc9be-edd5-4614-a7d0-e975915a15bc
