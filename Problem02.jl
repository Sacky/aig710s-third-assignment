### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 9c16d236-36d1-4c05-92ad-dbf6c22843b6
using Pkg

# ╔═╡ b7842947-d32f-4f41-92be-af08d497127c
Pkg.activate("Project.toml")

# ╔═╡ b5749229-4e18-43e9-b203-5db641f49db5
using PlutoUI

# ╔═╡ 26739861-2d1b-43ab-abad-229e24d2dda7
md"# Sequential Game"

# ╔═╡ 94073144-a381-4ff1-866e-8b69f1847a6b
md"##### Game board"

# ╔═╡ f7f4fa20-da53-11eb-1f01-9d0208f6e0b0
board = [[" 5, 4 ", " 2, 2 "],
         [" 1, 1 ", " 4, 5 "]]

# ╔═╡ e3a0cd0b-57bb-4787-9b3b-f016db74dd26
md"##### Variables"

# ╔═╡ 553a4464-d89f-40f4-8c49-1f6639ff9129
#player = [4 2 0; 0 3 2; 1 2 4]

# ╔═╡ 6744e9db-2a43-48f5-bd07-992ead2ee58e
player1 ="A"

# ╔═╡ 4ad46413-617a-404e-9408-975dee47fa15
player2 = "B"

# ╔═╡ 9db6031c-ebe7-43fa-b340-c8b0e3e165cb
player1_win = 1 # If player A has won

# ╔═╡ f9fde45b-685e-42f5-85a2-389ef40170b2
tie = 0 # If there are no more moves

# ╔═╡ 4bc29150-640f-422b-a4fd-59720d9b9af0
player2_win = -1 # If player B has won

# ╔═╡ 8ff85e4f-08ae-4a9b-b10c-27db96370771
md"##### Game State"

# ╔═╡ 3111aab4-2320-4c23-892c-072bddd9406e
md"##### Minimax algorithm to return Optimal Result"

# ╔═╡ 6681b256-bd6f-4892-b091-f1d9e70aa533
md"##### Normal Form Game's player with strategies "

# ╔═╡ c130a6e6-e031-4f15-ac0b-14ec588d2e08
function print_board(board)
	println("                 PlAYER2     ")
	println("")
	println("                X2     Y2    ")
	println("            -|---———|———---|")
    println("          X1 |",join(board[1],"|"))
    println("PlAYER1     -|---———┼———---|")
    println("          Y1 |",join(board[2],"|"))
    println("            -|---———┼———---|")
	println("")
end

# ╔═╡ 5403e30a-8b58-4432-8819-2b5baf656628
md"##### Payoff Matrix"

# ╔═╡ 147bb0a7-24f5-49fd-b3ad-6b3a8ab39a65
#A set of players (Player1 and Player2)

# ╔═╡ a50667d5-52ab-4a41-b417-52a4642797f2
#A set of strategies for each player accordingly(X1,X2 and Y1,Y2)

# ╔═╡ 82725368-a731-4173-94d7-9054ea329b06
#=
For each player, they have preferences over the set of strategy profiles:
{(X1,X2), (X1,Y2), (Y1,X2), (Y1,Y2)}
=#

# ╔═╡ 2e6100ec-b9e6-431c-a633-9f44a6d85b37
with_terminal() do
	print_board(board)
end

# ╔═╡ b8d13115-3bf2-4103-bcad-cf0d7bf97a82
md"##### Players Action (payoff)"

# ╔═╡ c376e05a-f9ff-402e-a930-9f93ac9c7b4b
function compute_payoff(x=player1, y=player2)
	if x == "X1" ; y == "X2"
		println("PLAYER1 {}".format(5), ":","PLAYER2 {}".format(4))
	elseif x == "X1" ; y == "Y2"
		println("PLAYER1 {}".format(2), ":","PLAYER2 {}".format(2))
	elseif x == "Y1" ; y == "X2"
		println("PLAYER1 {}".format(1), ":","PLAYER2 {}".format(1))
	else
		println("PLAYER1 {}".format(4), ":","PLAYER2 {}".format(5))
	end
end		

# ╔═╡ 0539628a-c162-44e1-b997-f26abec78697
function make_move!(board, player, (x, y))
    board[x][y] = player
end

# ╔═╡ 28e8b4eb-469c-4ba9-92c7-fba950d63d2d
function check_available(board)
    available_cells = []
    for i in 1:2, j in 1:2
        if board[i][j] == " "
            push!(available_cells, (i, j))
        end
    end
    return available_cells
end

# ╔═╡ 9e162441-ece3-409a-bc88-c960e70a7395
function payoff_check(board)
	 # Rows
    for i in 1:2
        if (board[i][1] == board[i][2]) && (board[i][1] in [player1, player2])
            if (board[i][1] == player1)
                return player1_win
            end
            return player2_win
        end
    end
	 # Column
    for i in 1:2
        if (board[1][i] == board[2][i]) && (board[1][i] in [player1, player2])
            if (board[1][i] == player1)
                return player1_win
            end
            return player2_win
        end
    end
	 # First Diagonals
    if (board[1][1] == board[2][2]) && (board[1][1] in [player1, player2])
        if (board[1][1] == player1)
            return player1_win
        end
        return player2_win
    end
     # Second Diagonals
    if (board[1][2] == board[2][2] == board[2][1]) && (board[1][2] in [player1, player2])
        if (board[1][2] == player1)
            return player1_win
        end
        return player2_win
    end

    # Tie
    if length(check_available(board)) == 0
        return tie
    end

    # No player wins, game continues
    return nothing
end

# ╔═╡ 5f6341c6-3e80-4daa-9a1a-3107192484f6
function minimax(board, depth, is_maximising)
    result = payoff_check(board)

    if result !== nothing
        return result
    elseif is_maximising

        best_score = -Inf

        for i in 1:2, j in 1:2
            if board[i][j] == " "

                board[i][j] = player2

                score = minimax(board, depth + 1, false)
                
                board[i][j] = " "

                best_score = max(score, best_score)
            end
        end

        return best_score        
    else        
        best_score = Inf

        for i in 1:2, j in 1:2
            if board[i][j] == " "

                board[i][j] = player1

                score = minimax(board, depth + 1, true)
                
                board[i][j] = " "

                best_score = min(score, best_score)
            end
        end

        return best_score   
    end

end

# ╔═╡ 1c7ee672-4a9a-4fd9-ae1d-4771b66674b0
function best_move(board)
    best_score = -Inf

    best_pos = (0, 0)

    for i in 1:2, j in 1:2
        if board[i][j] == " "

            board[i][j] = player2
            
            score = minimax(board, 0, false)

            
            board[i][j] = " "

            if score > best_score
                best_score = score
                best_pos = (i, j)
            end
        end
    end
    return best_pos
end

# ╔═╡ 522e5a5e-2113-4ed3-9ddb-4f1e4c7a8340
function wise_move!(board)
    make_move!(board, player2, best_move(board))
end

# ╔═╡ d0a1e094-3b70-4d95-8b5c-d21d3d77fedb
md"##### Play the Game"

# ╔═╡ 7629d228-db35-481b-b0c9-1cba88e7c82c
function play_game()
	while true
    print_board(board)

    print("Enter: ")
    txt = split(readline())
    make_move!(board, player1, (parse(Int, txt[1]), parse(Int, txt[2])))

    if payoff_check(board) == player1_win
        println(1)
        break
    elseif payoff_check(board) == player2_win
        println(2)
        break
    elseif payoff_check(board) == tie
        println("Tie!")
        break
    end

    ai_move!(board)
    print_board(board)

    if payoff_check(board) == player1_win
        println(1)
        break
    elseif payoff_check(board) == player2_win
        println(2)
        break
    elseif payoff_check(board) == tie
        println("Tie!")
        break
    end
end
end
	

# ╔═╡ 49b3786c-bde0-40ec-a247-3e9297f4ad13
#@bind txt TextField()

# ╔═╡ 18b5149c-cb87-4db6-bddf-2946e6174f7e
#txt

# ╔═╡ e1de16a3-5bfc-4f6d-8ac7-ca25c04c5e82
#@bind start_first Radio(["Yes","No"])

# ╔═╡ 3a51b542-e749-4aa3-8a86-3f713f318dd2
#start_first

# ╔═╡ 5e682266-d78d-45a0-ada3-5a5eb3723f56
#@bind status CheckBox()

# ╔═╡ 06cd8204-99ac-4307-9770-24beed53be26
#=if status
	md"5, 4"
end=#

# ╔═╡ 0322dbfc-46ae-4087-9725-ef8835df95c8
#=if status
	md"1,1"
end=#

# ╔═╡ Cell order:
# ╠═26739861-2d1b-43ab-abad-229e24d2dda7
# ╠═9c16d236-36d1-4c05-92ad-dbf6c22843b6
# ╠═b7842947-d32f-4f41-92be-af08d497127c
# ╠═b5749229-4e18-43e9-b203-5db641f49db5
# ╠═94073144-a381-4ff1-866e-8b69f1847a6b
# ╠═f7f4fa20-da53-11eb-1f01-9d0208f6e0b0
# ╠═e3a0cd0b-57bb-4787-9b3b-f016db74dd26
# ╠═553a4464-d89f-40f4-8c49-1f6639ff9129
# ╠═6744e9db-2a43-48f5-bd07-992ead2ee58e
# ╠═4ad46413-617a-404e-9408-975dee47fa15
# ╠═9db6031c-ebe7-43fa-b340-c8b0e3e165cb
# ╠═f9fde45b-685e-42f5-85a2-389ef40170b2
# ╠═4bc29150-640f-422b-a4fd-59720d9b9af0
# ╠═8ff85e4f-08ae-4a9b-b10c-27db96370771
# ╠═9e162441-ece3-409a-bc88-c960e70a7395
# ╠═3111aab4-2320-4c23-892c-072bddd9406e
# ╠═5f6341c6-3e80-4daa-9a1a-3107192484f6
# ╠═1c7ee672-4a9a-4fd9-ae1d-4771b66674b0
# ╠═6681b256-bd6f-4892-b091-f1d9e70aa533
# ╠═c130a6e6-e031-4f15-ac0b-14ec588d2e08
# ╠═5403e30a-8b58-4432-8819-2b5baf656628
# ╠═147bb0a7-24f5-49fd-b3ad-6b3a8ab39a65
# ╠═a50667d5-52ab-4a41-b417-52a4642797f2
# ╠═82725368-a731-4173-94d7-9054ea329b06
# ╠═2e6100ec-b9e6-431c-a633-9f44a6d85b37
# ╠═b8d13115-3bf2-4103-bcad-cf0d7bf97a82
# ╠═c376e05a-f9ff-402e-a930-9f93ac9c7b4b
# ╠═0539628a-c162-44e1-b997-f26abec78697
# ╠═28e8b4eb-469c-4ba9-92c7-fba950d63d2d
# ╠═522e5a5e-2113-4ed3-9ddb-4f1e4c7a8340
# ╠═d0a1e094-3b70-4d95-8b5c-d21d3d77fedb
# ╠═7629d228-db35-481b-b0c9-1cba88e7c82c
# ╠═49b3786c-bde0-40ec-a247-3e9297f4ad13
# ╠═18b5149c-cb87-4db6-bddf-2946e6174f7e
# ╠═e1de16a3-5bfc-4f6d-8ac7-ca25c04c5e82
# ╠═3a51b542-e749-4aa3-8a86-3f713f318dd2
# ╠═5e682266-d78d-45a0-ada3-5a5eb3723f56
# ╠═06cd8204-99ac-4307-9770-24beed53be26
# ╠═0322dbfc-46ae-4087-9725-ef8835df95c8
